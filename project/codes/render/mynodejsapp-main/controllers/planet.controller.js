const Planet = require('../models/planet.model');
function getPlanets (req, res) {
    Planet.find()
    .then((result) => {
        res.send(result);
    }).catch((err) => {res.status(500).send(err)});
}
function getPlanet (req, res) {
    Planet.findOne({_id: req.params.id})
    .then((result) =>{
        if (result) {
            res.send(result)
        } else {res.status(400).send(`Planet id ${req.params.id} does not exist`)}
    })
    .catch((err) => res.status(500).send(err));
}
function getPlanetbyColor (req, res) {
    Planet.find({color: req.params.color})
    .then((result) => {
        if (result) {
            res.send(result)
        } else {res.status(400).send(`Planet color ${req.params.color} does not exist`)}
    }).catch((err) => {res.status(500).send(err)});
}
function getPlanetbySize (req, res) {
    Planet.find({size: { $lt: req.params.size }})
    .then((result) => {
        if (result) {
            res.send(result)
        } else {res.status(400).send(`Planet with size less than ${req.params.size} does not exist`)}
    }).catch((err) => {res.status(500).send(err)});
}
function postPlanet (req, res) {
    if (!req.body.name) {
        return res.status(400).send('Missing planet\'s name');
    }
    const planet = new Planet({
        name: req.body.name,
        color: req.body.color,
        size: req.body.size,
        satellites: req.body.satellites
    });
    planet.save()
    .then((result) => {
        res.send(result);
    }).catch((err) => {
        res.status(500).send(err);
    });
}
function putPlanet (req, res) {
    if (!req.body.name) {
        return res.status(400).send('Missing planet\'s name');
    }
    Planet.findOneAndUpdate({_id: req.params.id}, {
        name: req.body.name,
        color: req.body.color,
        size: req.body.size,
        satellites: req.body.satellites
    }).then((result) => { // mongoose current bug can not send updated value
        res.send(result);
    }).catch((err) => {
        res.status(500).send(err);
    });
}
function deletePlanet (req, res) {
    Planet.findOneAndDelete({_id: req.params.id})
    .then((result) => {
        res.send(result);
    }).catch((err) => {res.status(500).send(err)});
}
function addSatellite (req, res) {
    Planet.findOne({_id: req.params.id})
    .then((result) =>{
        if (result) {
            const newSatellite = req.body;
            result['satellites'].push(newSatellite);
            Planet.findOneAndUpdate({_id: req.params.id}, {
                satellites: result['satellites'],
            }).then((rst) => { 
                res.send(rst);
            });
        } else {res.status(400).send(`Planet id ${req.params.id} does not exist`)}
    })
    .catch((err) => res.status(500).send(err));
}
module.exports = { getPlanets, getPlanet, getPlanetbyColor,
    getPlanetbySize, postPlanet,  putPlanet, deletePlanet, addSatellite}