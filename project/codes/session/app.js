const session = require('express-session');
const express = require('express');
const formidable = require('express-formidable');
const path = require('path');
const app = express()
const userData = { username: 'admin', password: 'admin' }

app.use(session({ resave: true, secret: 'mysecret', saveUninitialized: true}));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(formidable());

app.use((req, res, next) => {
    res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
});

app.get('/', (req, res) => {
    res.render('login');
});

app.post('/login', (req, res) => {
    if (req.fields.username === userData.username && req.fields.password === userData.password) {
        req.session.myusername = req.fields.username;
        res.redirect('/welcomepage');
    } else { res.send('wrong password'); }
});

app.get('/welcomepage', (req, res) => {
    if(req.session.myusername) {
        res.render('welcome', {data: req.session.myusername});
    } else { res.send('not logged in !'); }
});

app.post('/logout', (req, res) => {
    req.session.destroy();
    res.redirect('/');
});

app.listen(3000);