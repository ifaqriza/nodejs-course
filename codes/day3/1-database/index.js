require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const planetRouter = require('./routes/planet.router');
const app = express();
const PORT = 3000;
const jwt = require('jsonwebtoken');
const User = require('./models/user.model');
const auth = require('./auth');

mongoose.connect(process.env.DB_CONNECTION)
.then((res) => console.log('Connected to database'))
.then((res) => app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
}))
.catch((err) => console.log(err));

app.use(cors({
    origin: '*'
}))

app.use(express.json());
app.get('/login', (req, res) => {
    User.findOne({name: req.body.name, password: req.body.name})
    .then((result) => {
        const userobj = {name: req.body.name};
        const token = jwt.sign(userobj, process.env.SECRET_TOKEN);
        res.json({accessToken: token});
    })
    .catch((err) => {res.send(err)});
})
app.get('/', auth, (req, res) => { res.send('Welcome to my web server'); });
app.use('/planets', planetRouter);
