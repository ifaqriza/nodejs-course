require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const planetRouter = require('./routes/planet.router');
const studentRouter = require('./routes/student.router');
const app = express();
const PORT = 3000;

mongoose.connect(process.env.DB_CONNECTION)
.then((res) => console.log('Connected to database'))
.then((res) => app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
}))
.catch((err) => console.log(err));

app.use(express.json({strict : false}));
app.get('/', (req, res) => { res.send('Welcome to my web server'); });
app.use('/planets', planetRouter);
app.use('/students', studentRouter);
