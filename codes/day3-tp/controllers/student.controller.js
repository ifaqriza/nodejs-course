const Student = require('../models/student.model');

function getStudents (req, res) {
    Student.find()
    .then((result) => {
        res.send(result);
    }).catch((err) => {res.status(500).send(err)});
}
function getStudent (req, res) {
    Student.findOne({_id: req.params.id})
    .then((result) =>{
        if (result) {
            res.send(result)
        } else {res.status(400).send(`Student id ${req.params.id} does not exist`)}
    })
    .catch((err) => res.status(500).send(err));
}
function postStudent (req, res) {
    if (!req.body.name) {
        return res.status(400).send('Missing student\'s name');
    }
    const student = new Student({
        name: req.body.name,
        courses: req.body.courses
    });
    student.save()
    .then((result) => {
        res.send(result);
    }).catch((err) => {
        res.status(500).send(err);
    });
}
function course (req, res) {
    Student.findOne({_id: req.params.id})
    .then((result) =>{
        if (result) {
            const course = req.body;
            var key;
            for (var k in course) {
                key = k;
                break;
            }
            result['courses'][key] = course[key];
            Student.findOneAndUpdate({_id: req.params.id}, {
                courses: result['courses'],
            }).then((rst) => { 
                res.send(rst);
            });
        } else {res.status(400).send(`Student id ${req.params.id} does not exist`)}
    })
    .catch((err) => res.status(500).send(err));
}
function remove (req, res) {
    Student.findOne({_id: req.params.id})
    .then(async (result) =>{
        if (result) {
            const course = req.body;
            delete result['courses'][course];
            Student.findOneAndUpdate({_id: req.params.id}, {
                courses: result['courses']
            }).then((rst) => { 
                res.send(rst);
            });
        } else {res.status(400).send(`Student id ${req.params.id} does not exist`)}
    })
    .catch((err) => res.status(500).send(err));
}
module.exports = {getStudents, getStudent, postStudent, course, remove};