const mongoose = require('mongoose');
const studentSchema = mongoose.Schema({
    name: { type: String, required: true },
    courses: { type: Object }
}, { timestamps: true });
const Student = mongoose.model('Students', studentSchema);
module.exports = Student;