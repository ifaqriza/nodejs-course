module.exports = {
    ...require('./pathfinder'),
    ...require('./osinfo'),
    ...require('./urlsplitter'),
    ...require('./emitthis'),
}