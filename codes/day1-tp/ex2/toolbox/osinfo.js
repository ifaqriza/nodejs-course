const os = require('os');
function getOs() {
    console.log(`Platform: ${os.platform()}`);
}
module.exports = {
    getOs
}