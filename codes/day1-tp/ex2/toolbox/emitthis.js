const Events = require('events');

const user = new Events();
user.on('test1', () => {
    console.log('print test1');
})
user.on('test2', () => {
    console.log('print test2');
})
function userInput (test) {
    if (test == "test1") {
        user.emit("test1");
    } else if (test == "test2") {
        user.emit("test2")
    }
}

module.exports = {
    userInput
}