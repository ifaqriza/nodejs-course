var mypromise = (val) => {return new Promise((resolve, reject) => {
    if (val >= 0) {
        resolve(val);
    } else {
        reject(val);
    }
})};
const myparam = parseInt(process.argv[2]);
mypromise(myparam).then((val) => {
        console.log('Success the input is positive!');
        var valnew = val + 10;
        console.log(`${val} + 10 = ${valnew}`);
        return valnew;
    }).then((val) =>{
        var valnew = val * 5;
        console.log(`${val} x ${5} = ${valnew}`);
    }).finally(() => {
        console.log('Computation is done!');
    }).catch(() =>{
        console.log('Error, there are negative number(s)!');
    });


