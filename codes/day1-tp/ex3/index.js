const yup = require("yup");
const Employee = yup.object().shape({
    name: yup.string().matches(/^[A-Z]/).required(),
    age: yup.number().positive(),
    email: yup.string().email(),
    password: yup.string().matches(/^(?=.*[a-z])(?=.*[0-9])/),
});
const employee = {
    name: "Robert",
    age: 28,
    email: "robert@gmail.com",
    password: "mypass123"
};
Employee.validate(employee)
    .then((valid) => { console.log(valid)})
    .catch((err) => console.log(err));

const { v4: uuidv4 } = require('uuid');
console.log(`Unique ID : ${uuidv4()}`);

const dayjs = require('dayjs');
console.log(`Current date : ${dayjs()}`);
