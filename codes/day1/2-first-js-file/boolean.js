function or (a, b) {
    return a || b;
}
function and (a, b) {
    return a && b;
}
const a = process.argv[2];
const b = process.argv[3];
const res1 = or (a, b);
const res2 = and (a, b);
console.log(`a = ${a}`);
console.log(`b = ${b}`);
console.log(`a or b = ${res1}`);
console.log(`a and b = ${res2}`);