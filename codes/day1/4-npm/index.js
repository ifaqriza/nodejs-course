const validator = require('validator');
const axios = require('axios');
const lodash = require('lodash');
const email1 = 'foo@bar.com';
const email2 = 'bar.com';
// VALIDATOR validate emails
if(validator.isEmail(email1)) {
    console.log(`${email1} is valid email`);
} else { console.log(`${email1} is not valid email`); }
if(validator.isEmail(email2)) {
    console.log(`${email2} is valid email`);
} else { console.log(`${email2} is not valid email`); }
// VALIDATOR strong password
const pass = 'pass';
if(validator.isStrongPassword(pass)) {
    console.log(`${pass} is strong password`);
} else { console.log(`${pass} is not strong password`); }
// AXIOS
axios.get('https://jsonplaceholder.typicode.com/users')
    .then((res) => {
        console.log(res.data);
    });
// LODASH split array and sum array
console.log(lodash.chunk(['a', 'b', 'c', 'd'], 2));
console.log(lodash.sum([1,2,1,2,2]));

console.log('TEST CHANGE')