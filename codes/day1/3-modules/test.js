const operators = require('./operators');

const a = process.argv[2];
const b = process.argv[3];
const res1 = operators.or (a,b);
const res2 = operators.and (a,b);

console.log(`a or b = ${res1}`);
console.log(`a and b = ${res2}`);

console.log(`5 + 5 = ${(operators.add(5,5))}`);
console.log(`5 - 5 = ${(operators.subs(5,5))}`);
console.log(`5 * 5 = ${(operators.multi(5,5))}`);
console.log(`5 / 5 = ${(operators.div(5,5))}`);
