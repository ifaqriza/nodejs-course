// export all the functions inside each module
module.exports = {
    ...require('./logic'),
    ...require('./math'),
}