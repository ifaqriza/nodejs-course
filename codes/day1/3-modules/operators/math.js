function add (a,b) {
    return a+b;
}
function subs (a,b) {
    return a-b;
}
function multi (a,b) {
    return a*b;
}
function div (a,b) {
    return a/b;
}
module.exports = {
    add, subs, multi, div
}