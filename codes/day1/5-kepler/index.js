const {parse} = require('csv-parse');
const fs = require('fs');
const results = [];
function isHabitable(planet){
    return planet['koi_disposition'] === 'CONFIRMED'
        && planet['koi_insol'] > 0.36
        && planet['koi_insol'] < 1.11
        && planet['koi_prad'] < 1.6
}
fs.createReadStream('kepler.csv')
    .pipe(parse({
        columns: true,
        comment: '#'
    }))
    .on('data', (res) => {
        if (isHabitable(res)) {
            results.push(res);
        }
    })
    .on('error', (err) => {
        console.log('error reading data');
    })
    .on('end', () => {
        const planetNames = results.map((planet) => {
            return planet['kepler_name'];
        });
        console.log(planetNames);
        console.log(`${planetNames.length} habitable planets`);
        console.log('DONE');
    });;