const studentRouters = require('./routes/student.router');
const express = require('express');
const app = express();
const PORT = 3000;
app.use(express.json({ strict: false }))
app.use('/students', studentRouters);
app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
})

