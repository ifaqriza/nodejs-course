const express = require('express');
const studentController = require('../controllers/student.controller');
const studentRouter = express.Router();
studentRouter.get('/', studentController.getStudents);
studentRouter.get('/:id', studentController.getStudent);
studentRouter.post('/', studentController.addStudent);
studentRouter.put('/course/:id', studentController.course);
studentRouter.put('/remove/:id', studentController.remove);
module.exports = studentRouter;
