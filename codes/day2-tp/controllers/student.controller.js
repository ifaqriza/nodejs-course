const students = require('../models/students');
const uuid = require('uuid');

function getStudents (req, res) {
    res.send(students);
}

function getStudent (req, res) {
    const tmpid = req.params.id;
    const student = students.filter(obj => {
        return obj.id === tmpid
      });
    if (student) {
        res.send(student);
    } else {
        res.status(400).send(`No student with id ${id}`);
    }
}

function addStudent (req, res) {
    const student = req.body;
    student['id'] = uuid.v4();
    students.push(student);
    res.send(student);
}

function course (req, res) {
    const tmpid = req.params.id;
    const student = students.filter(({id}) => id === tmpid);
    if (student) {
        const idx = students.findIndex((obj => obj.id == tmpid));
        const course = req.body;
        var key;
        for (var k in course) {
            key = k;
            break;
        }
        student[0]['courses'][key] = course[key];
        students[idx] = student[0];
        res.send(student[0]);
    } else {
        res.status(400).send(`No student with id ${id}`);
    }
}

function remove (req, res) {
    const tmpid = req.params.id;
    const student = students.filter(({id}) => id === tmpid);
    if (student) {
        const idx = students.findIndex((obj => obj.id == tmpid));
        const course = req.body;
        delete student[0]['courses'][course];
        students[idx] = student[0];
        res.send(student[0]);
    } else {
        res.status(400).send(`No student with id ${id}`);
    }
}

module.exports = {getStudents, getStudent, addStudent, course, remove};