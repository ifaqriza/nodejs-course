const express = require('express');
const planetController = require('../controllers/planet.controller');
const planetRouter = express.Router();
planetRouter.get('/', planetController.getPlanets);
planetRouter.get('/:id', planetController.getPlanet);
planetRouter.get('/color/:color', planetController.getPlanetsbyColor);
planetRouter.get('/size/:size', planetController.getPlanetsbySize);
planetRouter.post('/', planetController.postPlanet);
planetRouter.put('/:id', planetController.putPlanet);
planetRouter.delete('/:id', planetController.deletePlanet);
module.exports = planetRouter;

