const express = require('express');
const cors = require('cors');
const path = require('path');
const planetRouter = require('./routes/planet.router');
const app = express();
const PORT = 3000;

app.use(cors({
    origin: '*'
}));
app.use(express.json());
app.get('/', (req, res) => {
    res.send('Welcome to my web server');
}); 
app.get('/messages', (req, res) => {
    res.send('<h1>Messages</h1><ul><li>Hello 1</li><li>Hello 2</li></ul>');
});

app.use('/planets', planetRouter);

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
})




// app.get('/picture', (req, res) => {
//     const mypath = path.join(__dirname, 'public', 'mypicture.jpg');
//     res.sendFile(mypath);
// });