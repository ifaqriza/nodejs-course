const {planets} = require('./planets');
const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 3000;

app.use(cors({
    origin : '*'
}));

app.use(express.json());

app.use((req, res, next) => {
    console.log(`A request is received`);
    next();
});
app.use((req, res, next) => {
    const start = Date.now();
    next();
    const delta = Date.now() - start;
    console.log(`Response time: ${delta}ms\n`);
});
app.use((req, res, next) => {
    console.log(`Request method: ${req.method}`+
    `\nUrl: ${req.url}\nHostname: ${req.hostname}\nIP: ${req.socket.remoteAddress}`);
    next();
});

app.get('/', (req, res) => {
    res.send('Welcome to my web server');
}); 
app.get('/messages', (req, res) => {
    res.send('<h1>Messages</h1><ul><li>Hello 1</li><li>Hello 2</li></ul>');
});
app.get('/planets', (req, res) => {
    res.send(planets);
});
app.get('/planets/:id', (req, res) => {
    const tmpid = req.params.id;
    const planet = planets.filter(({id}) => id === +tmpid);
    res.send(planet[0]);
});
app.get('/planets/color/:color', (req, res) => {
    const tmpcolor = req.params.color;
    const planet = planets.filter(({color}) => color == tmpcolor);
    if (planet.length > 0) {
        res.send(planet);
    } else {
        res.status(400).send(`No planet with color ${tmpcolor}`);
    }
});
app.get('/planets/size/:size', (req, res) => {
    const tmpsize = req.params.size;
    const planet = planets.filter(({size}) => size < tmpsize);
    if (planet.length > 0) {
        res.send(planet);
    } else {
        res.status(400).send(`No planet with size < ${tmpsize}`);
    }
});
app.post('/planets', (req, res) => {
    if (!req.body.name) {
        return res.status(400).send('Missing planet\'s name');
    }
    const newPlanet = {
        id: planets[planets.length - 1].id + 1,
        name: req.body.name,
        color: req.body.color,
        size: req.body.size
    }
    planets.push(newPlanet);
    res.send(newPlanet);
});
app.put('/planets/:id', (req, res) => {
    if (!req.body.name) {
        return res.status(400).send('Missing planet\'s name');
    }
    const tmpid = req.params.id;
    const planet = planets.filter(({id}) => id === +tmpid);
    if (planet.length[0]) {
        const idx = planets.findIndex((obj => obj.id == tmpid));
        const newPlanet = {
            id: +tmpid,
            name: req.body.name,
            color: req.body.color,
            size:req.body.size
        }
        planets[idx] = newPlanet;
        res.send(newPlanet);
    } else {
        res.status(400).send(`No planet with id ${tmpid}`);
    }
});
app.delete('/planets/:id', (req, res) => {
    const tmpid = req.params.id;
    const planet = planets.filter(({id}) => id === +tmpid);
    if (planet) {
        const idx = planets.findIndex((obj => obj.id == tmpid));
        planets.splice(idx, 1);
        res.send(`Planet with id ${tmpid} is deleted`);
    } else {
        res.send(`Planet with id ${tmpid} doesn't exist`);
    }
});

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
})
