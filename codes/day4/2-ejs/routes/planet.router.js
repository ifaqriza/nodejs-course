const express = require('express');
const planetController = require('../controllers/planet.controller');
const planetRouter = express.Router();
planetRouter.get('/', planetController.getPlanets);
planetRouter.post('/post', planetController.postPlanet);
planetRouter.post('/put', planetController.putPlanet);
planetRouter.post('/delete', planetController.deletePlanet);
module.exports = planetRouter;

