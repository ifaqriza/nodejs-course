const express = require('express');
const studentController = require('../controllers/student.controller');
const studentRouter = express.Router();
studentRouter.get('/', studentController.getPlanets);
studentRouter.post('/post', studentController.postPlanet);
studentRouter.post('/put', studentController.putPlanet);
studentRouter.post('/delete', studentController.deletePlanet);
module.exports = studentRouter;

