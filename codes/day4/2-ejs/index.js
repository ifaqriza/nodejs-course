require('dotenv').config();
const express = require('express');
const formidable = require('express-formidable');
const path = require('path');
const mongoose = require('mongoose');
const app = express();
const PORT = process.env.PORT || 3000;
const planetRouter = require('./routes/planet.router');
const studentRouter = require('./routes/student.router');

mongoose.connect(process.env.DB_CONNECTION)
.then((res) => console.log('Connected to database'))
.then((res) => app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
}))
.catch((err) => console.log(err));

app.use(formidable());
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.get('/', (req, res) => {
    res.render('index', {
        title: 'Welcome to my website.',
        content: 'This website is built using Node.js template engine, ejs.'
    });
});

app.use('/planets', planetRouter);
app.use('/students', studentRouter);