const Planet = require('../models/planet.model');
function getPlanets (req, res) {
    Planet.find()
    .then((result) => {
        res.render('planets', {
            data: result
        });
    });
}
function postPlanet (req, res) {
    const planet = new Planet({
        name: req.fields.name,
        color: req.fields.color,
        size: parseInt(req.fields.size)
    });
    planet.save()
    .then((result) => {
        res.redirect('/planets');
    }).catch((err) => {
        res.redirect('/planets');
    });
}
function putPlanet (req, res) {
    Planet.findOneAndUpdate({_id: req.fields.id}, {
        name: req.fields.name,
        color: req.fields.color,
        size: parseInt(req.fields.size)
    }).then((result) => {
        res.redirect('/planets');
    }).catch((err) => {
        res.redirect('/planets');
    });
}
function deletePlanet (req, res) {
    Planet.findOneAndDelete({_id: req.fields.id})
    .then((result) => {
        res.redirect('/planets');
    }).catch((err) => {
        res.redirect('/planets');
    });
}
module.exports = { getPlanets, postPlanet, putPlanet, deletePlanet}