const Student = require('../models/student.model');
function getStudents (req, res) {
    Student.find()
    .then((result) => {
        res.render('students', {
            data: result
        });
    });
}
function postStudent (req, res) {
    res.redirect('/students');
}
function putStudent (req, res) {
    res.redirect('/students');
}
function deleteStudent (req, res) {
    res.redirect('/students');
}
module.exports = { getStudents, postStudent, putStudent, deleteStudent }