const express = require('express');
const path = require('path');
const app = express();
const PORT = process.env.PORT || 3000;
const planetRouter = require('./routes/planet.router');
const formidable = require('express-formidable');

app.use(formidable());
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.get('/', (req, res) => {
    res.render('index', {
        title: 'Welcome to my website.',
        content: 'This website is built using Node.js template engine, ejs.'
    });
});

app.use('/planets', planetRouter);

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
})
