const myapi = "https://myfirstnodejsapp.onrender.com";
const axios = require('axios');
function getPlanets (req, res) {
    axios.get(`${myapi}/planets`)
    .then((result) => {
        res.render('planets', {
            data: result.data
        });
    });
}
function postPlanet (req, res) {
    axios.post(`${myapi}/planets`, {
        name: req.fields.name,
        color: req.fields.color,
        size: parseInt(req.fields.size)
    })
    .then((results) => {
        res.redirect('/planets');
    }).catch((err) => res.send(err));
}
function putPlanet (req, res) {
    axios.put(`${myapi}/planets/${req.fields.id}`, {
        name: req.fields.name,
        color: req.fields.color,
        size: parseInt(req.fields.size)
    })
    .then((results) => {
        res.redirect('/planets');
    }).catch((err) => res.send(err));
}
function deletePlanet (req, res) {
    axios.delete(`${myapi}/planets/${req.fields.id}`)
    .then((results) => {
        res.redirect('/planets');
    }).catch((err) => res.send(err));
}
module.exports = { getPlanets, postPlanet, putPlanet, deletePlanet}